[Unit]
Description=Minio server
Wants=network-online.target
After=network-online.target
AssertFileIsExecutable=/usr/local/bin/minio

[Service]
LimitNOFILE=65536
Type=simple
User=minio
Group=minio

Environment=MINIO_ACCESS_KEY=TEST_LOCAL
Environment=MINIO_SECRET_KEY=TEST_LOCAL
ExecStart=/usr/local/bin/minio server /var/lib/minio/data

Restart=on-failure

[Install]
WantedBy=multi-user.target
