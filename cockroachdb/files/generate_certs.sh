#! /bin/bash

mkdir certs key

cockroach cert create-ca \
--certs-dir=certs \
--ca-key=key/ca.key

cockroach cert create-node \
127.0.0.1 \
localhost \
stealth-vault \
--certs-dir=certs \
--ca-key=key/ca.key

cockroach cert create-client \
root \
--certs-dir=certs \
--ca-key=key/ca.key